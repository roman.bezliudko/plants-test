import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

public class GameImpl extends Game {
    private Screen gameScreen;

    @Override
    public void create() {
        gameScreen = new ScreenImpl(this);
        setScreen(gameScreen);
    }
}