import com.badlogic.gdx.backends.jglfw.JglfwApplication;

public class Application {
    public static int WINDOW_WIDTH = 800;
    public static int WINDOW_HEIGHT = 600;

    public static void main(String[] args) {
        new JglfwApplication(new GameImpl(), "Main window", WINDOW_WIDTH, WINDOW_HEIGHT);
    }
}